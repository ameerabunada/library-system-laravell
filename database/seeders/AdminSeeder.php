<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $admin = Admin::create([
            'name' => 'Super Admin',
            'email' => 'admin@app.com',
            'password' => Hash::make(12345),
        ]);
        $admin->assignRole(Role::findById(1, 'admin'));
    }
}
