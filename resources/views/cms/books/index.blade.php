@extends('cms.parent')

@section('title','Books')
@section('page-large-name','Books')
@section('page-small-name','Index')

@section('styles')

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Bordered Table</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Year</th>
                                    <th>Language</th>
                                    <th>Quantity</th>
                                    <th>Visible</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Settings</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($books as $book)
                                <tr>
                                    <td>{{$book->id}}</td>
                                    <td>
                                        <img class="img-circle img-bordered-sm" height="60" width="60"
                                            src="{{Storage::url('books/'.$book->image)}}" alt="user image">
                                    </td>
                                    <td>{{$book->name}}</td>
                                    <td>{{$book->category->name}}</td>
                                    <td>
                                        <span class="badge bg-info">{{$book->year}}</span></td>
                                    <td>
                                        <span class="badge bg-success">{{$book->language_name}}</span>
                                    </td>
                                    <td>
                                        <span class="badge bg-primary">({{$book->quantity}}) Book/s</span></td>
                                    </td>
                                    <td>
                                        <span class="badge bg-info">({{$book->visibility_status}})</span></td>
                                    </td>
                                    <td>{{$book->created_at->format('y-m-d H:ma')}}</td>
                                    <td>{{$book->updated_at->format('y-m-d H:ma')}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{route('books.edit',$book->id)}}" class="btn btn-info">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            @if ($book->trashed())
                                            <a onclick="deleteCategory({{$book->id}}, this)" class="btn btn-warning">
                                                <i class="fas fa-trash-restore"></i>
                                            </a>
                                            @else
                                            <a onclick="deleteCategory({{$book->id}}, this)" class="btn btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                            @endif

                                            {{-- DELETE | cms/admin/books/{book} --}}
                                            {{-- <form method="POST" action="{{route('books.destroy',$book->id)}}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                            </form> --}}
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">

                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection

@section('scripts')

<script>
    function deleteCategory(id, reference){
        confirmDestroy('/cms/admin/books',id, reference)
    }
</script>
@endsection

{{-- Swal.fire(
'Deleted!',
'Your file has been deleted.',
'success'
) --}}