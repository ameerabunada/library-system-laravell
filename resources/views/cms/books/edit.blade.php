@extends('cms.parent')

@section('title','DEMO')
@section('page-large-name','DEMO')
@section('page-small-name','Demo')

@section('styles')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('cms/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('cms/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Book</h3>
                    </div>

                    <!-- /.card-header -->
                    <!-- form start -->
                    <form>
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Category</label>
                                <select class="form-control categories" id="category_id" style="width: 100%;">
                                    {{-- <option selected="selected">Alabama</option> --}}
                                    @foreach ($categories as $category)
                                    <option value="{{$category->id}}" @if($category->id == $book->category_id) selected
                                        @endif>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" value={{$book->name}} id="name"
                                    placeholder="Enter name">
                            </div>
                            <div class="form-group">
                                <label for="description">Year</label>
                                <input type="number" class="form-control" value={{$book->year}} id="year"
                                    placeholder="Year">
                            </div>
                            <div class="form-group">
                                <label>Language</label>
                                <select class="form-control languages" id="language" style="width: 100%;">
                                    <option value="en" @if($book->language == 'en') selected @endif>English</option>
                                    <option value="ar" @if($book->language == 'ar') selected @endif>Arabic</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Quantity</label>
                                <input type="number" class="form-control" value='{{$book->quantity}}' id="quantity"
                                    placeholder="Quantity">
                            </div>
                            <div class="form-group">
                                <!-- <label for="customFile">Custom File</label> -->
                                <label for="description">Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="book_image">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="visible" id="visible"
                                        @if($book->is_visible) checked @endif>
                                    <label class="custom-control-label" for="visible">Visible</label>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="button" onclick="performUpdate()" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection

@section('scripts')
<!-- Select2 -->
<script src="{{asset('cms/plugins/select2/js/select2.full.min.js')}}"></script>
<script>
    //Initialize Select2 Elements
    $('.categories').select2({
        theme: 'bootstrap4'
    });
    $('.languages').select2({
        theme: 'bootstrap4'
    });

    function performUpdate(){
        // let data = {
        //     name: document.getElementById('name').value,
        //     year:document.getElementById('year').value,
        //     quantity:document.getElementById('quantity').value,
        //     category_id: document.getElementById('category_id').value,
        //     language: document.getElementById('language').value,
        //     visible: document.getElementById('visible').checked,
        // }

        let formData = new FormData();
        formData.append('_method','PUT');
        formData.append('name',document.getElementById('name').value);
        formData.append('year',document.getElementById('year').value);
        formData.append('quantity',document.getElementById('quantity').value);
        formData.append('category_id',document.getElementById('category_id').value);
        formData.append('language',document.getElementById('language').value);
        formData.append('visible',document.getElementById('visible').checked);
        if(document.getElementById('book_image').files[0] != undefined){
            formData.append('image',document.getElementById('book_image').files[0]);
        }
        store('/cms/admin/books/{{$book->id}}',formData);
    }
</script>
@endsection