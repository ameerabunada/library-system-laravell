@extends('cms.parent')

@section('title','Users')
@section('page-large-name','Users')
@section('page-small-name','Index')

@section('styles')

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Bordered Table</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Permissions</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Settings</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>
                                        <span class="badge bg-info">{{$user->email}}</span></td>
                                    <td>
                                        <a href="{{route('user-permissions.show',$user->id)}}"
                                            class="btn btn-block btn-info btn-sm">({{$user->permissions_count}})
                                            Permission/s</a>
                                    </td>
                                    <td>{{$user->created_at->format('y-m-d H:ma')}}</td>
                                    <td>{{$user->updated_at->format('y-m-d H:ma')}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{route('users.edit',$user->id)}}" class="btn btn-info">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a onclick="performDelete({{$user->id}}, this)" class="btn btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">

                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@endsection

@section('scripts')

<script>
    function performDelete(id, reference){
        confirmDestroy('/cms/admin/users',id, reference)
    }
</script>
@endsection

{{-- Swal.fire(
'Deleted!',
'Your file has been deleted.',
'success'
) --}}