<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RolePermissionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserPermissionController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes(['verify' => true]);

Route::prefix('cms/')->middleware('guest:admin,user')->group(function () {
    Route::get('{guard}/login', [AuthController::class, 'showLogin'])->name('cms.login');
    Route::post('login', [AuthController::class, 'login']);
});

Route::prefix('cms/admin')->middleware(['auth:admin', 'verified'])->group(function () {
    Route::resource('admins', AdminController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('permissions', PermissionController::class);
    Route::resource('permissions/role', RolePermissionController::class);
    Route::resource('permissions/user-permissions', UserPermissionController::class);
    Route::resource('users', UserController::class);
});

Route::prefix('cms/admin')->middleware(['auth:admin,user', 'verified'])->group(function () {
    Route::get('dashboard/{locale?}', [DashboardController::class, 'showDashboard']);
    Route::resource('books', BookController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('countries', CountryController::class);

    Route::get('change-password', [AuthController::class, 'showChangePassword'])->name('cms.change-password');
    Route::post('change-password', [AuthController::class, 'changePassword']);

    Route::get('logout', [AuthController::class, 'logout'])->name('cms.logout');
});

Route::get('/email/verify', function () {
    return view('cms.auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/cms/admin');
})->middleware(['auth:admin', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    // return back()->with('message', 'Verification link sent!');
    return response()->json(['message' => 'Verification link sent!']);
})->middleware(['auth:admin', 'throttle:6,1'])->name('verification.send');

// Route::middleware(['age:22','m2','m3'])->group(function () {
//     Route::get('news', function () {
//         return 'WELCOEM IN NEWS PAGE';
//     })->withoutMiddleware('m3');
// });

// Route::middleware('age:22')->get('news', function () {
//     return 'WELCOEM IN NEWS PAGE';
// });

// Route::get('news', function () {
//     return 'WELCOEM IN NEWS PAGE';
// })->middleware('age:22',);


// Route::get('news', function () {
//     return response()->json(['status' => 'success', 'active' => true]);
// })->middleware(CheckAge::class);
