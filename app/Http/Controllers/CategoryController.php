<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Category::class, 'category');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Category::withCount('books')->get();
        return response()->view('cms.categories.index', ['categories' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // $this->authorize('create', Category::class);
        return response()->view('cms.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $this->authorize('create', Category::class);
        $request->validate([
            'name' => 'required|string|min:3|max:40',
            'description' => 'nullable|string|min:3|max:50',
            'visible' => 'in:on|string'
        ], [
            'name.required' => 'Please, enter category name'
        ]);
        $category = new Category();
        $category->name = $request->get('name');
        $category->description = $request->get('description');
        $category->is_visible = $request->has('visible');
        $isSaved = $category->save();
        if ($isSaved) {
            session()->flash('type', 'success');
            session()->flash('message', 'Category saved successfully');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
        // $this->authorize('view', [$category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Category::find($id);
        $this->authorize('update', [Category::find($id)]);
        return response()->view('cms.categories.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $request->validate([
            'name' => 'required|string|min:3|max:20',
            'description' => 'nullable|string|min:3|max:50',
            'visible' => 'in:on'
        ]);

        $category->name = $request->get('name');
        $category->description = $request->get('description');
        $category->is_visible = $request->has('visible');
        $isSaved = $category->save();
        if ($isSaved) {
            return redirect()->route('categories.index');
        } else {
            session()->flash('message', 'Update failed!');
            session()->flash('type', 'danger');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
        $deleted = $category->delete();
        if ($deleted) {
            return response()->json(['title' => 'Deleted successfully', 'icon' => 'success']);
        } else {
            return response()->json(['title' => 'Delete failed', 'icon' => 'danger']);
        }
        // if ($deleted) {
        //     return redirect()->back();
        // }
    }
}
