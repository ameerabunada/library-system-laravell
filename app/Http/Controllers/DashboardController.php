<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class DashboardController extends Controller
{
    //
    public function showDashboard($locale = 'en')
    {
        App::setLocale($locale);

        $booksCount = Book::count();
        return response()->view('cms.dashboard', ['booksCount' => $booksCount]);
    }
}
