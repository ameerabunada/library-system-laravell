<?php

namespace App\Http\Controllers\API;

use App\Helpers\Messages;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class ApiAuthController extends Controller
{
    //
    // public function login(Request $request)
    // {
    //     $validator = Validator($request->all(), [
    //         'email' => 'required|email|string|exists:users,email',
    //         'password' => 'required|string',
    //     ]);

    //     if (!$validator->fails()) {
    //         $user = User::where('email', $request->get('email'))->first();
    //         if (Hash::check($request->get('password'), $user->password)) {
    //             //CREATE-TOKEN
    //             if (!$this->activeLogins($user->id)) {
    //                 $token = $user->createToken('User-Token');
    //                 $user->setAttribute('token', $token->accessToken);
    //                 return response()->json([
    //                     'status' => true,
    //                     'message' => 'Logged in successfully',
    //                     'data' => $user,
    //                     // 'token' => $token
    //                 ]);
    //             } else {
    //                 return response()->json([
    //                     'status' => false,
    //                     'message' => 'Unable to login from two devices at the same time.',
    //                 ]);
    //             }
    //         } else {
    //             return response()->json(['message' => 'Login failed, wrong credentials'], Response::HTTP_BAD_REQUEST);
    //         }
    //     } else {
    //         return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
    //     }
    // }

    // public function login(Request $request)
    // {
    //     $validator = Validator($request->all(), [
    //         'email' => 'required|email|string|exists:users,email',
    //         'password' => 'required|string',
    //     ]);

    //     if (!$validator->fails()) {
    //         $user = User::where('email', $request->get('email'))->first();
    //         if (Hash::check($request->get('password'), $user->password)) {
    //             //REVIOKE ACTIVE TOKENS
    //             $this->revokeActiveTokens($user->id);
    //             //CREATE-TOKEN
    //             $token = $user->createToken('User-Token');
    //             $user->setAttribute('token', $token->accessToken);
    //             return response()->json([
    //                 'status' => true,
    //                 'message' => 'Logged in successfully',
    //                 'data' => $user,
    //                 // 'token' => $token
    //             ]);
    //         } else {
    //             return response()->json(
    //                 ['message' => 'Login failed, wrong credentials'],
    //                 Response::HTTP_BAD_REQUEST
    //             );
    //         }
    //     } else {
    //         return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
    //     }
    // }

    public function login(Request $request)
    {
        $validator = Validator($request->all(), [
            'email' => 'required|email|string|exists:users,email',
            'password' => 'required|string',
        ]);

        if (!$validator->fails()) {
            //CREATE Password Grant Token - Supports Mutli Auth - Use Provider

            return $this->generatePasswordGrantToken($request);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }
    }

    private function generatePasswordGrantToken(Request $request)
    {
        try {
            $response = Http::asForm()->post('http://127.0.0.1:8001/oauth/token', [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'Biu4K7Ves9ZzqToZ0puN5Z8tLfaAm1K9XdHgfSP6',
                'username' => $request->get('email'),
                'password' => $request->get('password'),
                'scope' => '*'
            ]);

            $user = User::where('email', $request->get('email'))->first();
            $user->setAttribute('token', $response->json()['access_token']);
            $user->setAttribute('token_type', $response->json()['token_type']);
            return response()->json([
                'status' => true,
                'message' => Messages::getMessage('LOGGED_IN_SUCCESSFULLY'),
                'data' => $user,
            ]);
        } catch (\Exception $exception) {
            $message = '';

            if ($response['error'] == 'invalid_grant') {
                $message = 'Login failed, wrong credentials';
            } else {
                $message = 'Something went wrong, please try again!';
            }
            return response()->json(
                [
                    'status' => false,
                    'message' => $message
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    public function activeLogins($userId)
    {
        return DB::table('oauth_access_tokens')
            ->where('user_id', $userId)
            ->where('name', 'User-Token')
            ->where('revoked', false)
            ->count();
    }

    public function revokeActiveTokens($userId)
    {
        DB::table('oauth_access_tokens')
            ->where('user_id', $userId)
            ->where('name', 'User-Token')
            ->update(['revoked' => true]);
    }

    public function logout(Request $request)
    {
        $token = auth('api')->user()->token();
        $revoked = $token->revoke();
        return response()->json([
            'status' => $revoked, 'message' => $revoked ? 'Logged out successfully' : 'Failed to logout'
        ], $revoked ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
    }
}
