<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $books = Book::all();
        // $books = Book::paginate(1);
        $books = Book::simplePaginate();
        return response()->json([
            'status' => true, 'message' => 'Success', 'data' => $books
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:100',
            'year' => 'required|numeric|digits:4',
            'language' => 'required|string|in:en,ar',
            'quantity' => 'required|integer|min:1',
            'visible' => 'required|boolean',
            'image' => 'nullable|image|mimes:jpg,png|size:2048',
            'category_id' => 'required|integer|exists:categories,id'
        ]);

        if (!$validator->fails()) {
            $book = new Book();
            $book->name = $request->get('name');
            $book->year = $request->get('year');
            $book->language = $request->get('language');
            $book->quantity = $request->get('quantity');
            $book->category_id = $request->get('category_id');
            // Category::findOrFail($request->get('category_id'))->books()->save($book);
            $isSaved = $book->save();

            return response()->json([
                'message' => $isSaved ? 'Saved successfully' : 'Failed to create new book!'
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:100',
            'year' => 'required|numeric|digits:4',
            'language' => 'required|string|in:en,ar',
            'quantity' => 'required|integer|min:1',
            'visible' => 'required|boolean',
            'image' => 'nullable|image|mimes:jpg,png|size:2048',
            'category_id' => 'required|integer|exists:categories,id'
        ]);

        if (!$validator->fails()) {
            $book->name = $request->get('name');
            $book->year = $request->get('year');
            $book->language = $request->get('language');
            $book->quantity = $request->get('quantity');
            $book->category_id = $request->get('category_id');
            // Category::findOrFail($request->get('category_id'))->books()->save($book);
            $isSaved = $book->save();

            return response()->json([
                'message' => $isSaved ? 'Saved successfully' : 'Failed to create new book!'
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // $deleted = Book::destroy($id);
        $deleted = Book::findOrFail($id)->delete();
        return response()->json([
            'status' => $deleted, 'message' => $deleted ? 'Deleted successfully' : 'Delete failed',
        ], $deleted ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
    }
}
