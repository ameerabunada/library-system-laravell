<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $admins = Admin::where('id', '!=', auth('admin')->id())->get();
        return response()->view('cms.admins.index', ['admins' => $admins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::where('guard_name', 'admin')->get();
        return response()->view('cms.admins.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'role_id' => 'required|integer|exists:roles,id',
            'name' => 'required|string|min:3|max:40',
            'email' => 'required|email|string|unique:admins,email',
        ]);

        if (!$validator->fails()) {
            $role = Role::findById($request->get('role_id'), 'admin');
            $admin = new Admin();
            $admin->name = $request->get('name');
            $admin->email = $request->get('email');
            $admin->password = Hash::make(12345);
            $isSaved = $admin->save();
            if ($isSaved) {
                event(new Registered($admin));
                $admin->assignRole($role);
            }
            return response()->json(['message' => $isSaved ? 'Admin saved successfully' : 'Failed to save admin'], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
        $assignedRole = $admin->getRoleNames()[0];
        $roles = Role::where('guard_name', 'admin')->get();
        return response()->view('cms.admins.edit', ['admin' => $admin, 'roles' => $roles, 'assignedRole' => $assignedRole]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
        $validator = Validator($request->all(), [
            'role_id' => 'required|integer|exists:roles,id',
            'name' => 'required|string|min:3|max:40',
            'email' => 'required|email|string|unique:admins,email,' . $admin->id,
        ]);


        if (!$validator->fails()) {
            $role = Role::findById($request->get('role_id'), 'admin');
            $admin->name = $request->get('name');
            $admin->email = $request->get('email');
            $isSaved = $admin->save();
            if ($isSaved) {
                // if (!$admin->hasRole($role)) {
                //     $admin->assignRole($role);
                // }
                $admin->syncRoles($role);
            }
            return response()->json(['message' => $isSaved ? 'Admin saved successfully' : 'Failed to save admin'], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
        $deleted = $admin->delete();
        if ($deleted) {
            return response()->json(['title' => 'Deleted successfully', 'icon' => 'success']);
        } else {
            return response()->json(['title' => 'Delete failed', 'icon' => 'danger']);
        }
    }
}
