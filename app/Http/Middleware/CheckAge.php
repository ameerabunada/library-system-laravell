<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $age)
    {
        // $age = 22;
        if ($age > 20) {
            return $next($request);
        } else {
            abort(403, 'Your age is less than 20');
        }

        // $response = $next($request);
        // if ($age > 20) {
        //     if ($response->active) {
        //         return $response;
        //     } else {
        //         abort(403, 'Your account is not active');
        //     }
        // } else {
        //     abort(403, 'Your age is less than 20');
        // }


        //BEFORE-MIDDLEWARE
        //CHECKS
        //PASS -> return $next($request);
        //FAILED -> REDIRECT

        // $response = $next($request);
        //AFTER-MIDDLEWARE
        // return $response;
    }
}
