<?php

namespace App\Policies;

use App\Models\Admin;
use App\Models\Category;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Admin  $admin
     * @return mixed
     */
    public function viewAny($userId)
    {
        //
        $guard = auth('admin')->check() ? 'admin' : 'user';
        return auth($guard)->user()->hasPermissionTo('Read-Categories')
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Admin  $admin
     * @param  \App\Models\Category  $category
     * @return mixed
     */
    public function view(Admin $admin, Category $category)
    {
        //
        return $this->deny('DENIED');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Admin  $admin
     * @return mixed
     */
    public function create($userId)
    {
        //
        $guard = auth('admin')->check() ? 'admin' : 'user';
        if (auth('user')->check()) return $this->deny();
        return auth($guard)->user()->hasPermissionTo('Create-Category')
            ? $this->allow()
            : $this->deny('UNAUTHORIZED ACTION | FORBIDDEN');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Admin  $admin
     * @param  \App\Models\Category  $category
     * @return mixed
     */
    public function update(Admin $admin, Category $category)
    {
        //
        if (auth('user')->check()) return $this->deny();
        return auth('admin')->user()->hasPermissionTo('Update-Category')
            ? $this->allow()
            : $this->deny('UNAUTHORIZED ACTION | FORBIDDEN');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Admin  $admin
     * @param  \App\Models\Category  $category
     * @return mixed
     */
    public function delete(Admin $admin, Category $category)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Admin  $admin
     * @param  \App\Models\Category  $category
     * @return mixed
     */
    public function restore(Admin $admin, Category $category)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Admin  $admin
     * @param  \App\Models\Category  $category
     * @return mixed
     */
    public function forceDelete(Admin $admin, Category $category)
    {
        //
    }
}
