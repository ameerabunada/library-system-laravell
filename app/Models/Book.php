<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use HasFactory, SoftDeletes;


    public function getVisibilityStatusAttribute()
    {
        return $this->is_visible ? 'Visible' : 'Hidden';
    }


    public function getLanguageNameAttribute()
    {
        return $this->language == 'en' ? 'English' : 'Arabic';
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
